import numpy as np
import os
import glob
import pandas as pd
import time
import natsort
import warnings
import matplotlib.pyplot as plt
from scipy import optimize as opt
from inspect import signature
import datetime
# import warnings

"""
LTATools is a custom module that was developed to help with the analysis of
phosphorescence studies performed on the EZTime.
"""

# ------------------------------
# Internal constants
VERSION = 'LTATools_7.3.0'
LTA_EXE = 'C:\\Users\\Public\\AnalysisCode\\lifetime_analyzer_3-3-2.exe'
CH = ['Hpo', 'Eug', 'Hpr', 'Ref', 'Nan']  # order and name of channels
NAS = 'N:\\EZ_Data'  # nas dir

# ------------------------------
# useful collection of consts that I use all the time
C = [
    "tab:blue", "tab:orange", "tab:green", "tab:red", 'tab:purple',
    'tab:brown', 'tab:pink', 'tab:gray', 'tab:olive', 'tab:cyan']
CM = ["#0072bd", "#d95319", "#edb120", "#7e2f8e"]

US = r'$\tau$ ($\mu$s)'
AMP = 'Amplitude (counts)'

CONST_O2 = 0.062504  # [(mol/m^3)/(mg/L)]
CONST_GLU = 0.055507  # [(mol/m^3)/(mg/deciliter)]
CONST_O234 = 0.33  # divide probe values by this to get [O2%]


# ------------------------------
# Useful funs
def TimeIt(f):
    def Timed(*args, **kw):
        t0 = time.time()
        fun = f(*args, **kw)
        t1 = time.time()

        print(f'Exec time: {t1 - t0:.4f} s')
        return fun
    return Timed


def LookupEZ(EZN):
    f = f'N:\\EZ_Data\\Processed\\Data\\DATA-EZ_{EZN}-EZRUN_*.csv'
    return glob.glob(f)


def TrimTimeStamp(tDir):
    """
    Examines data and the timestamps, and eliminates timestamp entries with no
    data. Useful since the timestamps happens before data acquisition, and thus
    can potentially log before any data is taken. Can happen when script is
    manually terminated.
    """
    h5 = glob.glob('*.h5')
    h5 = [i.split('.')[0] for i in h5]

    tfile = tDir.split('\\')[-1]
    tfile = f'{tDir}\\{tfile}_TIMESTAMPS.csv'
    ts = pd.read_csv(tfile, header=None)
    ts = ts.set_index('scan')


def SetSort(s):
    """
    Takes a list like, and returns a natsorted array of unique values
    """
    return np.array(natsort.natsorted(list(set(s))))


def ReadLTAOut(f):
    '''
    Function to read LTA output csvs.

    It is detached from all of the modules here so that we can read in csv
    files directly without any class instantiation, avoiding the computational
    and cranial overhead associated with it, in case of custom scans.
    '''
    model = f.split('\\')[-1].split('_')[1]

    df = pd.read_csv(f)
    k = np.array([
        i.split('\\')[-1].split('.')[0]
        for i in df['File'].values], dtype=str)

    df['File'] = k
    df = df.set_index('File')

    # remove keys we don't care about
    df = df.drop(
        ['Device', 'Model', 'Sequence', 'Navg', 'Ts', 'Te', 'Version'], axis=1)
    return df, model


def ReadConfigTable(sensors, TABLE=None) -> dict:
    """
    Takes a list-like of sensors and will search the card overview table in
    order to match it to a build config. Returns this as a dict.

    TABLE is optional and should point to the config table. Defaults to N:
    """
    def FindConfigTable() -> str:
        ctab = glob.glob('N:\\Config_Data\\mobi_card_overview_v*.xlsx')
        if len(ctab) == 0:
            # raise Exception('Card config xlsx not found')
            return False

        ctab = natsort.natsorted(ctab)[-1]
        return ctab

    def Configs(table) -> pd.core.frame.DataFrame:
        # this gets the config sheets
        if table is False:
            head = [
                'Card', 'ID', 'BID/MID', 'Sensor start', 'Sensor end',
                'Tag card', 'Tag sensor', 'RC OSP ID', 'RC OSP Geom',
                'RC OSP Process', 'Ref OSP ID', 'Ref OSP Geom',
                'Ref OSP Process', 'RC Label', 'RC GOx ID', 'RC GOx process',
                'RC GOx Cure', 'Ref Fill ID', 'Conduit process', 'Conduit ID',
                'Liner Replace', 'Port ID', 'Singulation ID', 'OA Type']

        ezc = pd.read_excel(pd.ExcelFile(table), 'Full Ez Config')
        for i in ezc:  # ffill everything
            ezc[i] = ezc[i].fillna(method='ffill')
        return ezc

    if TABLE is None:
        TABLE = FindConfigTable()

    # ------------------------------
    err_mislabel = 'Labeling of sensor start and end is ambiguous'

    ezc = Configs(TABLE)  # config table

    # return ezc
    # init dict of groups
    gdict = {}

    # loop over tested sensors:
    for i in sensors:
        card, sen = i.split('-')
        card, sen = int(card), int(sen)

        ezci = ezc[ezc['Card ID'] == card].copy()  # subselect for card
        if len(ezci) > 0:
            # enforce numerical ordering
            ezci.sort_values('Sensor start', ignore_index=True, inplace=True)

            # last value that is true is where our sensor is
            line = ezci[ezci['Sensor start'] <= sen].iloc[-1]
            # double check everything is kosher
            if line['Sensor start'] >= sen > line['Sensor end']:
                raise Exception(err_mislabel)

            # assemble dict entry
            gdict[i] = (line['Tag card'], line['Tag sensor'])
            # gdict[i] = (
            #     line['Tag card'].replace(', ', ' '),
            #     line['Tag sensor'].replace(', ', ' '))
        else:
            warnings.warn(f'Card {card} has no config entry')
            gdict[i] = ('', '')

    return gdict


def GetNScan(df):
    x = [i for i in df.keys() if '.' in i]
    if len(x) == 0:
        return 1

    x = set([int(j.split('.')[1]) for j in x])
    return max(x) + 1


def SaveDF(df, ezid, ezrun, model, sDir='.', **kwargs) -> None:
    """
    Saves data frame. Be sure to use this function in order to enforce a
    common format.

    DATA-EZ_XXX-EZRUN_YYYYYY-AtBC.csv
    """
    df.to_csv(f'{sDir}\\DATA-{ezid}-{ezrun}-{model}.csv', **kwargs)


def LoadDF(tDir):
    """
    Loads a data frame. Pass it the full path to the csv.
    Also returns the fit model. See also SaveDF.
    """
    model = tDir.split('.')[0].split('-')[-1]
    df = pd.read_csv(tDir, index_col=0)
    return df, model


def Subselect(df, gdict) -> pd.core.frame.DataFrame:
    """
    Function for subselecting data from a pandas dataframe based on a
    collection of criteria.

    Pass it the dataframe and a dictionary whose:
        gdict = {"column_to_sort": value_to_select}
    value_to_select can be a single value or list like of multiple values

    Passing it a key that does not exist in the df will raise an assert
    """
    warn0 = "Key in dictionary not found in dataframe. Is there a typo?"

    m = np.ones(len(df), dtype=bool)
    enforce = (
        str, int, float, bool, np.int32, np.float32, np.int64, np.float64)
    for k in gdict:
        # - - - - - - - - - -
        assert k in df, warn0  # make sure entry actually exists
        gk = gdict[k]

        # - - - - - - - - - -
        if isinstance(gk, enforce):  # enforce list like
            gk = [gk]

        # - - - - - - - - - -
        mm = np.zeros(len(df), dtype=bool)  # select across key values
        for i in gk:
            warn1 = f"{i} not found in {k}. Is there a typo?"
            assert i in df[k].values, warn1  # make sure entry actually exists
            mm |= (df[k] == i)

        # - - - - - - - - - -
        m &= mm  # select across keys

    return(df[m])


def SepO2Glu(df):
    """
    Pass
        Full processed data frame

    Returns
        o2: setpoint o2 values (in ascending order, NOT test order)
            0% will be taken as the true zero, never the 0% @ 0mg level
        do2: dose index for said setpoint, in order of o2
        gl: see o2 but for glucose, true zero not included
        dgl: see do2 but for glucose
    """
    def ReadNomu(s):
        o, g = s.split('_')
        g = int(g[:-2])
        o = o.split('PC')
        o = float(o[0]) + (float(o[1]) * 0.1)
        return o, g

    df = df[df.Dosing > -1]
    lvl = df.nomu.values[:df.Dosing.max() + 1]

    o2, gl, do2, dgl = [], [], [], []
    for dose, i in enumerate(lvl):
        o, g = ReadNomu(i)
        # print(o, g)
        if i == '00PC0_000MG':
            o2 += [0.01]
            do2 += [dose]
        elif (o == 0) & (g > 0):  # true zero
            o2 = [0] + o2
            do2 = [dose] + do2
        elif (o > 0) & (g == 0):  # o2 level
            o2 += [o]
            do2 += [dose]
        elif g > 0:  # gl level
            gl += [g]
            dgl += [dose]

    # sort
    s = np.argsort(o2)
    o2 = np.array(o2)
    do2 = np.array(do2)
    o2 = o2[s]
    do2 = do2[s]
    s = np.argsort(gl)
    gl = np.array(gl)
    dgl = np.array(dgl)
    gl = gl[s]
    dgl = dgl[s]

    return [o2, do2, gl, dgl]


def InvMap(sensors):
    sen = np.array([i[:-2] for i in sensors])
    conf = ReadConfigTable(sen)
    cinv = {}
    for k in conf:
        c = f'{conf[k][0]}; {conf[k][1]}'
        if c not in cinv:
            cinv[c] = []
        cinv[c] += [k]

    m = np.zeros([len(cinv), len(sen)], dtype=bool)
    for i, k in enumerate(cinv):
        for s in cinv[k]:
            m[i] += s == sen
    return m, list(cinv.keys())


def Compare(dfl, exclude=[], include=[]):
    """
    Takes a list of EZTime data frames and returns out the common levels,
    sensors, and groups between them. Also subselects for said things.

    Pass
        dfl: list of processed dataframes
        exclude: list of excluded sensors
        include: list of sensors to exclusively analyze
            will override exclude if both are not None and raise a warning

    Returns
        subselected df, common nomu, common tag_u, group mask
    """

    if (len(include) > 0) & (len(exclude) > 0):
        warnings.warn(
            '\nBoth EXCLUDE and INCLUDE were defined. Overriding EXCLUDE.\n')

    # read in data sets
    sens = []
    lvls = []
    for i in range(len(dfl)):
        dfl[i] = dfl[i][dfl[i].Dosing > -1]
        sens += [set(dfl[i]['Sensor'])]
        lvls += [set(dfl[i]['nomu'])]

    # get out common levels and sensors only
    sens = sorted(sens[0].intersection(*sens))
    lvls = sorted(lvls[0].intersection(*lvls))
    for i in range(len(dfl)):
        dfl[i] = Subselect(dfl[i], {'Sensor': sens, 'nomu': lvls})

        # include and exclude
        if len(include) > 0:
            dfl[i] = Subselect(dfl[i], {'Sensor': include})
        elif len(exclude) > 0:
            for ex in exclude:
                dfl[i] = dfl[i][dfl[i].Sensor != ex]

    # separate out sensors by group
    grps = SetSort(dfl[0].tag_u)
    m = np.empty([len(grps), len(dfl[0])], dtype=bool)
    for i in range(len(grps)):
        m[i] = dfl[0].tag_u == grps[i]

    return dfl, lvls, grps, sens, m


def ParseCommon(tailor, exclude=[]):
    s = [i.A.sensors for i in tailor]
    g = [SetSort(i.A.df.tag_u.values) for i in tailor]

    gcom = [set(i) for i in g]
    gcom = np.array(sorted(gcom[0].intersection(*gcom[1:])))
    scom = [set(i) for i in s]
    scom = np.array(sorted(scom[0].intersection(*scom[1:])))

    m = [np.zeros([len(gcom), i.x.shape[0]], dtype=bool) for i in tailor]

    for di in range(len(tailor)):
        df = Subselect(tailor[di].A.df, {'Dosing': 0, 'Ch': 'Hpo'})
        msi = np.zeros(len(df), dtype=bool)
        mgi = np.zeros([len(gcom), len(df)], dtype=bool)

        for i in range(len(df)):
            loc = df.iloc[i]

            sen = loc.Sensor
            grp = loc.tag_u

            # mask for common sensors, and also exclude
            msi[i] = sen in scom
            for j in exclude:
                if j in sen:
                    msi[i] = False
                    break

            # check for common groups
            mgi[gcom == grp, i] = True

        m[di] = msi * mgi

    # some diagnostics code
    # df0 = Subselect(tailor[0].A.df, {'Dosing': 0, 'Ch': 'Hpo'})
    # df1 = Subselect(tailor[1].A.df, {'Dosing': 0, 'Ch': 'Hpo'})

    # for i in range(len(gcom)):
    #     df0i = df0[m[0][i]]
    #     df1i = df1[m[1][i]]
    #     print(df0i.Sensor.values == df1i.Sensor.values)

    # trim out common arrays that are exluded to nothing
    gcomt = gcom[m[0].sum(axis=1) > 0]

    scomt = [i[m[0].sum(axis=0) > 0] for i in s]
    scomt = [set(i) for i in scomt]
    scomt = np.array(sorted(scomt[0].intersection(*scomt[1:])))

    mt = [i[m[0].sum(axis=1) > 0] for i in m]

    return gcom, scom, m, gcomt, scomt, mt


def PlotFitCurves(T, save=True):
    # fit curve evaluator
    pw, ph = 10, 8
    fs = np.array([pw, ph]) * 1.1
    F = FigTools(figsize=fs)
    F.Grid()

    # - - - - - - - - - -
    # this could probably be more efficient, but meh
    # scatter plots
    s = T.A.sensors

    # padding and offsets
    pad = 1.35
    xmin = np.nanmin(T.x)
    xmax = np.nanmax(T.x)
    ymin = np.nanmin(T.y)
    ymax = np.nanmax(T.y)

    dx = xmax - xmin
    dy = ymax - ymin

    par = len(signature(T.fitEqu).parameters) - 1

    nx = 50
    xx = np.linspace(xmin, xmax, nx)
    nanpad = np.array([[np.nan] * len(s)]).T

    xxx = np.array(list(xx) * len(s)).reshape([len(s), nx])
    xxx -= xmin

    for ch in range(T.x.shape[1]):
        x_ = T.x[:, ch, :].copy() - xmin
        y_ = T.y[:, ch, :].copy() - ymin
        xx_ = np.append(xxx, nanpad, axis=1)

        h_ = np.empty([len(s), nx])
        for i in range(len(s)):
            h_[i] = T.fitEqu(xx, *T.h[i, ch, :par])
            h_[i][h_[i] < 0] = 120
            if T.fitEqu.__name__ == 'Line':
                h_[i] = 1 / h_[i]

            # now add in linear offsets
            x_[i] += int(i / ph) * pad * (dx)
            y_[i] += (i % ph) * pad * (dy)
            xx_[i] += int(i / ph) * pad * (dx)
            h_[i] += (i % ph) * pad * (dy)

        h_ -= ymin
        h_ = np.append(h_, nanpad, axis=1).reshape(-1)
        # h_[h_ < 0] = 5
        # xx_ = np.append(xx_, nanpad, axis=1).reshape(-1)

        F.g.scatter(x_, y_, marker='.', c=CM[ch], alpha=0.5)
        F.g.plot(xx_.reshape(-1), h_, c=CM[ch], alpha=0.75)

    # - - - - - - - - - -
    # sensor labels
    for i in range(len(s)):
        F.g.text(np.nanmean(x_[i]),
                 np.nanmax(y_[(i % ph)]) + (dy * 0.2),
                 s[i], va='center', ha='center', fontsize=8)

    # - - - - - - - - - -
    # ticks adjust
    x_ = x_.reshape([len(s), T.nlvl])
    xt = x_[::ph, [0, -1]].reshape(-1)
    xl = T.x[::ph, 0, [0, -1]].reshape(-1)
    xl = [f'{i:.1f}' for i in xl]
    F.g.set_xticks(xt)
    F.g.set_xticklabels(xl, rotation=70)
    F.g.set_xlabel('Reference')

    yt = y_.reshape([-1, T.nlvl])
    yt = yt[:ph, [0, -1]].reshape(-1)
    yl = T.y[:ph, 0, [0, -1]].reshape(-1)
    yl = [f'{i:.1f}' for i in yl]
    F.g.set_yticks(yt)
    F.g.set_yticklabels(yl)
    F.g.set_ylabel('Measured')

    # - - - - - - - - - -
    # other glams
    csv = T.csv.split('\\')[-1].split('-')
    t_ = f'Fits-{csv[1]}-{csv[2]}-{T.fitEqu.__name__}'
    F.f.suptitle(t_)
    F.f.tight_layout()
    if save:
        F.f.savefig(t_ + '.png')


def Glucose():
    s = '''
          CH2O               Roses are red,
          |_ _ _ O           violets are blue.
         / .   .  \          Donuts are sweet,
    O _ /          \ _ O     cuz I am too.
        \   \__/   /      
         \ _ _ _  /
          |      |
          O      O
    '''
    return s


# ------------------------------
# Modules
class Models:
    # equations for various models for the data
    def SV2(self, ox, t0, k1, k2):
        term = k1 * ox
        term += k2 * (ox ** 2)
        term += 1 / t0
        return term ** -1

    def SV2Inv(self, tau, t0, k1, k2):
        term = 4 * k2 * ((1 / t0) - (1 / tau))
        term = np.sqrt((k1 ** 2) - term)
        den = 2 * k2
        term = (-k1 + term) / den
        term[term < 0] = 0  # numerical error
        return term

    def Line(self, ox, t0=160, kq=2.3e-03):
        return (kq * ox) + (1 / t0)

    def LineInv(self, tau, t0=160, kq=2.3e-03):
        return (tau - (1 / t0)) / kq

    def PPP(self, glu, vm, gsg, k, ex=2.9):
        # ex = 2.9
        glu += np.finfo(float).eps

        term = glu ** ex
        term /= (k * glu) + (glu ** ex)

        term = 1 - term
        term *= vm - gsg
        term += gsg

        return term

    def PPPInv(self, ox, vm, gsg, k, ex=2.9):
        # will not correct for pathological values
        # ex = 2.9
        ex = 1 - ex

        term = ox - gsg
        term /= k * (vm - ox + np.finfo(float).eps)

        # z = (ox - gsg) / (vm - gsg)
        # print(z)
        # z = 1 - z

        # term = z * k / (1 - z + np.finfo(float).eps)
        # print(term)

        # try:
        #     term[term < 0] = 0
        #     term[term > 87884] = 87884
        # except TypeError:
        #     if term < 0:
        #         term = 0
        #     elif term > 87884:  # 400 mg/dL ** 1.9
        #         term = 87884

        # term += np.finfo(float).eps
        term **= 1 / ex

        return term


class TailorSV:
    def __init__(self, A, fitType=0, refit=False, w=True):
        if isinstance(A, str):
            A = AnalysisUtils(A)
        elif isinstance(A, AnalysisUtils):
            None
        ssrt = "Pass AnalysisUtils or dir to csv"
        assert isinstance(A, AnalysisUtils), ssrt
        ssrt = 'Pick linear, first, second order SV'
        assert fitType in [0, 1, 2], ssrt

        # ------------------------------
        # get out relevant attributes
        p = SepO2Glu(A.df)
        self.nomx = p[0]
        self.p = p[1]
        self.nlvl = len(self.p)
        self.nch = len(SetSort(A.df.Ch))
        self.csv = A.tDir
        self.test = self.csv.split('-')[1]

        if self.nomx[0] == 0:
            self.true0 = True
        else:
            self.true0 = False

        # - - - - - - - - - -
        # other inits
        self.fitType = fitType
        self.fitEqu = [
            Models().Line,
            # Models().SV,
            Models().SV2,
        ][fitType]
        self.fitInv = [
            Models().LineInv,
            # Models().SVInv,
            Models().SV2Inv,
        ][fitType]

        self.fkey = ['t0', 'kq', 'k2', 'et0', 'ekq', 'ek2']

        self.x, self.y = A.GetRefTau(self.p)
        self.x = self.x.reshape([-1, self.nlvl])
        self.y = self.y.reshape([-1, self.nlvl])

        # ==========
        # level order correction
        # the following 5 lines assume that the final level is true 0
        # if true0:
        p = self.p - self.p.min()
        p[p == p.max()] = -1
        self.x = self.x[:, p]
        if self.true0:
            self.x[:, 0] = 0
        self.y = self.y[:, p]
        # ==========

        # - - - - - - - - - -
        # Look for existence of parameters, dont fit if we don't need to
        if ~refit:
            if 't0' not in A.df.keys():
                refit = True

        if refit:
            # init dataframe to include fit keys and null them
            print(f'Fitting Oxygen data to SV{self.fitType}')
            for i in self.fkey:
                A.df[i] = -1

            # loop fit, then record into the dataframe
            for i, (xi, yi) in enumerate(zip(self.x, self.y)):
                hi = self.Fit(xi, yi)
                m = A.df.Sensor == A.sensors[int(i / 3)]
                m &= A.df.Ch == CH[i % 3]
                for j, jk in enumerate(self.fkey):
                    A.df.loc[m, jk] = hi[j]

            # now that we have fits, get the O2 prediction
            A.df = self.SolveStack(A.df)
            if w:
                A.df.to_csv(self.csv)

        # - - - - - - - - - -
        # get out the dosing values of h, which is len of x
        df = Subselect(A.df, {'Dosing': self.p[0]})
        self.h = df.loc[:, self.fkey].values

        # same for predicted o2
        df = Subselect(A.df, {'Dosing': self.p})
        self.o = df[f'DrYO2'].values
        self.o = self.o.reshape([-1, self.p.shape[0]])
        self.o = self.o[:, p]

        # reshape
        self.x = self.x.reshape([-1, self.nch, self.p.shape[0]])
        self.y = self.y.reshape([-1, self.nch, self.p.shape[0]])
        self.o = self.o.reshape([-1, self.nch, self.p.shape[0]])
        self.h = self.h.reshape([-1, self.nch, len(self.fkey)])
        self.A = A

    def Fit(self, ox, tau):
        # oxygen in %
        h = np.zeros(len(self.fkey)) - 1

        try:
            if self.fitType == 0:
                if self.true0:
                    z = opt.curve_fit(
                        lambda o2, kq: Models().Line(o2, tau[0], kq),
                        ox, 1 / tau, 5e-03)
                    h[0] = tau[0]
                    h[1] = z[0]
                    h[3] = -1
                    h[4] = np.sqrt(z[1])

                else:
                    z = opt.curve_fit(
                        Models().Line, ox, 1 / tau, (80, 5e-03),
                        bounds=(np.array([50, 0]),
                                np.array([200, 100])))
                    h[[0, 1]] = z[0]
                    h[[3, 4]] = np.sqrt(z[1].diagonal())

            elif self.fitType == 1:
                z = opt.curve_fit(
                    Models().SV, ox, tau, (160, 2.3e-03),
                    bounds=(np.array([tau[0] * 0.5, 0]),
                            np.array([tau[0] * 1.5, 10])))
                h[[0, 1]] = z[0]
                h[[3, 4]] = np.sqrt(z[1].diagonal())

            elif self.fitType == 2:
                # prone to failures
                z = opt.curve_fit(
                    Models().SV2, ox, tau,
                    bounds=(np.array([tau[0] * 0.5, 0, 0]),
                            np.array([tau[0] * 1.5, 10, 10])))
                h[[0, 1, 2]] = z[0]
                h[[3, 4, 5]] = np.sqrt(z[1].diagonal())

        except ValueError:
            return h
        return h

    def SolveStack(self, df, x=None):
        xflag = False
        if x is None:
            x = df.t1.values
            xflag = True

        if self.fitType == 0:
            y = Models().LineInv(1 / x, df.t0.values, df.kq.values)
        elif self.fitType == 1:
            y = Models().SVInv(x, df.t0.values, df.kq.values)
        elif self.fitType == 2:
            y = Models().SV2Inv(
                x, df.t0.values, df.kq.values, df.k2.values)

        if xflag:
            df[f'DrYO2'] = y
            return df
        else:
            return y


class TailorSV0:
    def __init__(self, A, fitType=0, refit=False, w=True, true0=True):
        if isinstance(A, str):
            A = AnalysisUtils(A)
        elif isinstance(A, AnalysisUtils):
            None
        ssrt = "Pass AnalysisUtils or dir to csv"
        assert isinstance(A, AnalysisUtils), ssrt
        ssrt = 'Pick linear, first, second order SV'
        assert fitType in [0, 1, 2], ssrt

        # ------------------------------
        # get out relevant attributes
        p = SepO2Glu(A.df)
        self.nomx = p[0]
        self.p = p[1]
        self.nlvl = len(self.p)
        self.nch = 3
        self.csv = A.tDir
        self.test = self.csv.split('-')[1]

        # - - - - - - - - - -
        # other inits
        self.fitType = fitType
        self.fitEqu = [
            Models().Line,
            Models().SV,
            Models().SV2,
        ][fitType]
        self.fitInv = [
            Models().LineInv,
            Models().SVInv,
            Models().SV2Inv,
        ][fitType]

        self.fkey = ['t0', 'kq', 'k2', 'et0', 'ekq', 'ek2']

        self.x, self.y = A.GetRefTau(self.p)
        self.x = self.x.reshape([-1, self.nlvl])
        self.y = self.y.reshape([-1, self.nlvl])

        # ==========
        # level order correction
        # the following 5 lines assume that the final level is true 0
        p = self.p - self.p.min()
        p[p == p.max()] = -1
        self.x = self.x[:, p]
        if true0:
            self.x[:, 0] = 0
        self.y = self.y[:, p]
        # ==========

        # - - - - - - - - - -
        # Look for existence of parameters, dont fit if we don't need to
        if ~refit:
            if 't0' not in A.df.keys():
                refit = True

        if refit:
            # init dataframe to include fit keys and null them
            print(f'Fitting Oxygen data to SV{self.fitType}')
            for i in self.fkey:
                A.df[i] = -1

            # loop fit, then record into the dataframe
            for i, (xi, yi) in enumerate(zip(self.x, self.y)):
                hi = self.Fit(xi, yi)
                m = A.df.Sensor == A.sensors[int(i / 3)]
                m &= A.df.Ch == CH[i % 3]
                for j, jk in enumerate(self.fkey):
                    A.df.loc[m, jk] = hi[j]

            # now that we have fits, get the O2 prediction
            A.df = self.SolveStack(A.df)
            if w:
                A.df.to_csv(self.csv)

        # - - - - - - - - - -
        # get out the dosing values of h, which is len of x
        df = Subselect(A.df, {'Dosing': self.p[0]})
        self.h = df.loc[:, self.fkey].values

        # same for predicted o2
        df = Subselect(A.df, {'Dosing': self.p})
        self.o = df[f'DrYO2'].values
        self.o = self.o.reshape([-1, self.p.shape[0]])
        self.o = self.o[:, p]

        # reshape
        self.x = self.x.reshape([-1, self.nch, self.p.shape[0]])
        self.y = self.y.reshape([-1, self.nch, self.p.shape[0]])
        self.o = self.o.reshape([-1, self.nch, self.p.shape[0]])
        self.h = self.h.reshape([-1, self.nch, len(self.fkey)])
        self.A = A

    def Fit(self, ox, tau):
        # oxygen in %
        h = np.zeros(len(self.fkey)) - 1

        try:
            if self.fitType == 0:
                z = opt.curve_fit(
                    Models().Line, ox, 1 / tau, (160, 2.3e-03),
                    bounds=(np.array([tau[0] * 0.5, 0]),
                            np.array([tau[0] * 1.5, 10])))
                h[[0, 1]] = z[0]
                h[[3, 4]] = np.sqrt(z[1].diagonal())

            elif self.fitType == 1:
                z = opt.curve_fit(
                    Models().SV, ox, tau, (160, 2.3e-03),
                    bounds=(np.array([tau[0] * 0.5, 0]),
                            np.array([tau[0] * 1.5, 10])))
                h[[0, 1]] = z[0]
                h[[3, 4]] = np.sqrt(z[1].diagonal())

            elif self.fitType == 2:
                # prone to failures
                z = opt.curve_fit(
                    Models().SV2, ox, tau,
                    bounds=(np.array([tau[0] * 0.5, 0, 0]),
                            np.array([tau[0] * 1.5, 10, 10])))
                h[[0, 1, 2]] = z[0]
                h[[3, 4, 5]] = np.sqrt(z[1].diagonal())

        except ValueError:
            return h
        return h

    def SolveStack(self, df, x=None):
        xflag = False
        if x is None:
            x = df.t1.values
            xflag = True

        if self.fitType == 0:
            y = Models().LineInv(1 / x, df.t0.values, df.kq.values)
        elif self.fitType == 1:
            y = Models().SVInv(x, df.t0.values, df.kq.values)
        elif self.fitType == 2:
            y = Models().SV2Inv(
                x, df.t0.values, df.kq.values, df.k2.values)

        if xflag:
            df[f'DrYO2'] = y
            return df
        else:
            return y


class TailorGlu:
    def __init__(self, O, glu, fitType=0, refit=False, w=True):

        ssrt = "Pass the TailorSV of the data"
        assert isinstance(O, TailorSV), ssrt
        ssrt = 'Pick exponential'
        assert fitType in [0], ssrt

        # ------------------------------
        # include the 0g level
        self.O = O
        self.A = O.A
        p = list(SepO2Glu(O.A.df))
        self.p = np.append(p[1][-1], p[3])
        self.nomx = np.append(0, p[2])

        # get out relevant attributes
        self.nlvl = len(self.p)
        self.nch = 3
        self.csv = O.A.tDir
        self.test = self.csv.split('-')[1]

        # - - - - - - - - - -
        # other inits
        self.fitType = fitType
        self.fitEqu = [
            Models().PPP,
        ][fitType]
        self.fitInv = [
            Models().PPPInv,
        ][fitType]

        self.fkey = ['vm', 'gsg', 'k',
                     'evm', 'egsg', 'ek']

        self.y0, self.t = O.A.GetRefTau(self.p)
        # self.t = self.t.reshape([-1, self.nlvl])
        # self.y0 = self.y0.reshape([-1, self.nlvl])
        df = Subselect(O.A.df, {'Dosing': self.p})
        self.y = df[f'DrYO2'].values
        self.y = self.y.reshape([-1, self.p.shape[0]])
        # self.y = self.y0 - self.y

        self.x = np.empty_like(self.y)
        for i in range(self.x.shape[0]):
            self.x[i] = glu

        for i in range(self.x.shape[0]):
            self.x[i] = glu

        # - - - - - - - - - -
        # Look for existence of parameters, dont fit if we don't need to
        if ~refit:
            if 'vm' not in O.A.df.keys():
                # print('derp')
                refit = True

        if refit:
            # init dataframe to include fit keys and null them
            print(f'Fitting Glucose data to Curve{self.fitType}')
            for i in self.fkey:
                O.A.df[i] = -1

            # loop fit, then record into the dataframe
            for i, (xi, yi) in enumerate(zip(self.x, self.y)):
                hi = self.Fit(xi, yi)
                m = O.A.df.Sensor == O.A.sensors[int(i / 3)]
                m &= O.A.df.Ch == CH[i % 3]
                for j, jk in enumerate(self.fkey):
                    O.A.df.loc[m, jk] = hi[j]

            if w:
                O.A.df.to_csv(self.csv)

        # - - - - - - - - - -
        # get out the dosing values of h, which is len of x
        df = Subselect(O.A.df, {'Dosing': self.p[0]})
        self.h = df.loc[:, self.fkey].values

        # reshape
        self.x = self.x.reshape([-1, self.nch, self.p.shape[0]])
        self.y = self.y.reshape([-1, self.nch, self.p.shape[0]])
        # self.o = self.o.reshape([-1, self.nch, self.p.shape[0]])
        self.h = self.h.reshape([-1, self.nch, len(self.fkey)])

    def Fit(self, glu, ox):
        h = np.zeros(len(self.fkey)) + 1

        try:
            if self.fitType == 0:
                # z = opt.curve_fit(
                #     Models().PPP, glu, ox, (1, 0.2, 1000),
                #     bounds=((0.1, 0, 100), (2, 2, 10000))
                # )

                vm = ox[0]
                gsg = ox[-1]
                z = opt.curve_fit(
                    lambda x, k: Models().PPP(x, vm, gsg, k, 2.9),
                    glu, ox, 4000,
                    bounds=(1e1, 1e6)
                )
                h[0] = vm
                h[1] = gsg
                h[2] = z[0]
            # if self.fitType == 0:
            #     h[[0, 1, 2]] = z[0]
            #     h[[3, 4, 5]] = np.sqrt(z[1].diagonal())

        except ValueError:
            None
        return h

    def SolveStack(self, x):
        if self.fitType == 0:
            try:
                y = Models().PPPInv(
                    x, self.O.A.df.vm.values, self.O.A.df.k.values)
            except TypeError:
                y = np.ones_like(x) * 120
        return y


class TailorGlu0:
    def __init__(self, A, glu, fitType=0, refit=False, w=True):
        if isinstance(A, str):
            A = AnalysisUtils(A)
        elif isinstance(A, AnalysisUtils):
            None
        ssrt = "Pass AnalysisUtils or dir to csv"
        assert isinstance(A, AnalysisUtils), ssrt
        ssrt = 'Pick Richards'
        assert fitType in [0], ssrt

        self.A = A

        # ------------------------------
        # include the 0g level
        p = list(SepO2Glu(A.df))
        self.p = np.append(p[1][-1], p[3])
        self.nomx = np.append(0, p[2])

        # get out relevant attributes
        self.nlvl = len(self.p)
        self.nch = 3
        self.csv = A.tDir
        self.test = self.csv.split('-')[1]

        # - - - - - - - - - -
        # other inits
        self.fitType = fitType
        self.fitEqu = [
            Models().Richard,
        ][fitType]
        self.fitInv = [
            Models().RichardInv,
        ][fitType]

        self.fkey = ['L', 'U', 'T', 'tm', 'kg',
                     'eL', 'eU', 'eT', 'etm', 'ekg']

        self.x, self.y = A.GetRefTau(self.p)  # we ar emerely recycling x
        self.x = self.x.reshape([-1, self.nlvl])
        self.y = self.y.reshape([-1, self.nlvl])

        for i in range(self.x.shape[0]):
            self.x[i] = glu

        # - - - - - - - - - -
        # Look for existence of parameters, dont fit if we don't need to
        if ~refit:
            if 'kg' not in A.df.keys():
                refit = True

        if refit:
            # init dataframe to include fit keys and null them
            print(f'Fitting Glucose data to Curve{self.fitType}')
            for i in self.fkey:
                A.df[i] = -1

            # loop fit, then record into the dataframe
            for i, (xi, yi) in enumerate(zip(self.x, self.y)):
                hi = self.Fit(xi, yi)
                m = A.df.Sensor == A.sensors[int(i / 3)]
                m &= A.df.Ch == CH[i % 3]
                for j, jk in enumerate(self.fkey):
                    A.df.loc[m, jk] = hi[j]

            if w:
                A.df.to_csv(self.csv)

        # - - - - - - - - - -
        # get out the dosing values of h, which is len of x
        df = Subselect(A.df, {'Dosing': self.p[0]})
        self.h = df.loc[:, self.fkey].values

        # same for predicted o2
        df = Subselect(A.df, {'Dosing': self.p})
        self.o = df[f'DrYO2'].values
        self.o = self.o.reshape([-1, self.p.shape[0]])
        # self.o = self.o[:, p]

        # reshape
        self.x = self.x.reshape([-1, self.nch, self.p.shape[0]])
        self.y = self.y.reshape([-1, self.nch, self.p.shape[0]])
        self.o = self.o.reshape([-1, self.nch, self.p.shape[0]])
        self.h = self.h.reshape([-1, self.nch, len(self.fkey)])

    def Fit(self, ox, tau):
        def FitRichard(x, y):
            h = np.zeros(len(self.fkey)) - 1
            L, U = y.min(), y.max()
            T = 20
            tm = 20
            k = 0.03

            i = 0.0
            while True:
                try:
                    bnd = np.array([L, U, T, tm, k])
                    z = opt.curve_fit(
                        Models().Richard, x, y,
                        bnd * np.array([1, 1, 1 + i, 1 + i, 1 + i]),
                        method='trf',
                        # max_nfev=3000,
                        bounds=(np.array([0, 50, 0, 1, 0.0]),
                                np.array([150, 200, 50, 1000, 0.1])))
                    break
                except RuntimeError:
                    if i >= 1:
                        print('Failure to find solution')
                        return np.zeros(len(self.fkey)) - 1
                    elif i > 0:
                        i *= -1
                    else:
                        i *= -1
                        i += 0.1

            h[:len(bnd)] = z[0]
            h[len(bnd):] = np.sqrt(z[1].diagonal())
            return h

        try:
            if self.fitType == 0:
                return FitRichard(ox, tau)

        except ValueError:
            return np.zeros(len(self.fkey)) - 1

    def SolveStack(self, x):
        if self.fitType == 0:
            try:
                y = Models().RichardInv(
                    x, self.A.df.L.values, self.A.df.U.values,
                    self.A.df.T.values, self.A.df.tm.values,
                    self.A.df.kg.values)
            except TypeError:
                y = np.ones_like(x) * 120
        return y


class FigTools:
    def __init__(self, show=True, **kwargs):
        self.f = plt.figure(**kwargs)
        self.show = show

    def GridCbar(self, dmin, dmax, discrete=False,
                 row=1, col=1, label='', width=0.1, **kwargs):
        """
        This sets up the gridspec but also defines a separate ax for the
        colorbar, which is self.gc

        Will also setup self.g as normal

        This fun assumes that each of your subplots will be using the same
        colorbar to describe the data it is plotting.

        dmin = equivalent to vmin for contour plots
        dmax = equivalent to vmax for contour plots
        label = axis label for the colorbar
        width = the width ratio relative to the other plots for the cbar
        """

        def Draw(ax):
            # draw said cbar
            if discrete is False:
                xx, yy = np.meshgrid(
                    np.linspace(dmin, dmax, 5),
                    np.linspace(dmin, dmax, 100))
                zz = np.empty_like(xx)
                for i, j in zip(
                    range(len(zz)), np.linspace(dmin, dmax, 100)):
                    zz[i] = j
            else:
                xx, yy = np.meshgrid(
                    np.linspace(dmin, dmax, 5),
                    np.linspace(dmin, dmax, discrete))
                zz = np.empty_like(xx)
                for i, j in zip(
                    range(len(zz)), np.linspace(dmin, dmax, discrete)):
                    zz[i] = j

            ax.pcolor(xx, yy, zz, shading='auto', **kwargs)

        # - - - - - - - - - -
        # set up array of gridsepc
        self.g = np.empty(row * col, dtype=object)

        # add in extra col for cbars
        wr = [1] * col + [width]
        gs = self.f.add_gridspec(row, col + 1, width_ratios=wr)

        # slice arrays differently if 1 vs multi row grids
        if row == 1:
            self.gc = self.f.add_subplot(gs[-1])
        else:
            self.gc = self.f.add_subplot(gs[:, -1])

        # instantiate the other grids
        kg = 0
        kgc = 0
        for i in range(row):
            for j in range(col):
                self.g[kg] = self.f.add_subplot(gs[kgc])
                kg += 1
                kgc += 1
            kgc += 1

        # - - - - - - - - - -
        Draw(self.gc)

        # flip labels on cbar
        self.gc.set_ylabel(label)
        self.gc.yaxis.set_label_position('right')
        self.gc.yaxis.tick_right()
        self.gc.get_xaxis().set_visible(False)

        # call this later, since the flipping messes with stuff
        # self.f.tight_layout()
        if self.show:
            self.f.show()

    def Grid(self, row=1, col=1, **kwargs):
        # - - - - - - - - - -
        # simply init all grids
        self.g = self.f.add_gridspec(row, col).subplots(**kwargs)
        if self.show:
            self.f.show()

    def CullInnerTicks(self):
        for i in self.g:
            if i.is_first_col() is False:
                i.get_yaxis().set_visible(False)
            if i.is_last_row() is False:
                i.get_xaxis().set_visible(False)

    def LabelOuter(self):
        for i in self.g:
            if i.is_first_col() is False:
                i.get_yaxis().set_visible(False)
            if i.is_last_row() is False:
                i.get_xaxis().set_visible(False)

    def WaterMarks(self, v, f, exe=LTA_EXE, vlta=VERSION, ps=(0.01, 0.93)):
        # version and execution watermarks
        s = v

        if exe == LTA_EXE:
            vLTA = exe.split('_')[-1][:-4]
            s += f'\nLTA v{vLTA}; {vlta}\n'
        else:
            s += f'\n{exe}; {vlta}\n'

        s += str(datetime.datetime.now())[:-7]
        f.text(*ps, s, c='gray', alpha=0.6)


class LTA:
    '''
    This class does the interfacing with LTA.
    The intended use is that each instance of LTA refers to a single experiment
    and a single model.

    In general, one should not have to call this directly, unless a unquiely
    custom experiment has been performed

    m = number of exponentials
    direct = directory to pull the analysis
    delta_t = time index where the tail fit starts, use defaults
    figs = yes/no LTA figures
    squelch = silence LTA's extra logs
    filt = semi-regex statement for including data (*'s only)
    '''

    def __init__(
            self, wd=os.getcwd(),
            m=1, start=None, sat=10000, filt=None, figs=False, squelch=True,
            lta=LTA_EXE):

        # - - - - - - - - - -
        # set directories
        self.LTAEXE = lta  # location of lta
        self.WD = wd  # working directory, typically the EZRUN folder
        self.PD = wd + '\\Processed'

        # - - - - - - - - - -
        # set local declares
        self.m = m
        self.sat = sat
        self.start, self.filt = self.WhatMachine(start, filt)
        self.figs = figs
        self.squelch = squelch
        self.model = self.Model(self.m, self.start, self.sat)  # fit model

        # - - - - - - - - - -
        # attributes that will cross talk with LTATools modules
        # this is the folder that gets made by the LTA
        self.fileCSV = f'{self.PD}' \
            f"\\lifetime_{self.model.replace(':', '-')}.csv"

    def WhatMachine(self, start=None, filt=None):
        '''
        Check which machine we used. We check by looking in this folder and
        Looks for .h5 files. If none exist, then we assume QM was used.
        These are machine specific constants and are already tuned!
        '''
        if start is not None and filt is not None:
            # if defined, do nothing
            return (start, filt)

        # else, check existing data sets to figure out which test system
        if 'EZ_' in self.WD:  # then this is EZTime
            default_start = 185
            default_filt = '.xlsx'
        # elif 'EZ_' in self.WD:  # then this is EZTime
        #     default_start = 185
        #     default_filt = '.h5'
        else:
            default_start = 120
            default_filt = '.txt'
            default_start = 185
            default_filt = '.xlsx'

        # check for overrides
        if start is None:
            start = default_start
        if filt is None:
            filt = default_filt

        return (start, filt)

    def Model(self, m, start, sat) -> str:
        # constructs the command line input for the fit model
        # Assert that we only fit to available models in the analyzer
        assert 1 <= m < 4, "Analyzer only fits 1-3 exponentials."
        return 'At' * m + f'BC_{start}_{sat}:Inf_-10'

    def Run(self) -> None:
        # - - - - - - - - - -
        # set LTA command line inputs
        cmd_str = f'{self.LTAEXE} -i {self.WD} -o {self.PD}'  # io locations

        # append model
        cmd_str += f' -m {self.model}'

        # set other options
        cmd_str += f' -n{self.filt} -l -a'
        if self.figs is True:  # draw figs, default no
            cmd_str += ' -f'
        if self.squelch is False:  # squelch cmd LTA outs, defualt yes
            cmd_str += ' -g'

        # - - - - - - - - - -
        # time to run LTA, delete matlab temps if necessary
        try:
            os.system(cmd_str)
        except ValueError:
            tempf = f'{os.getenv("temp")}\\{os.geteng("username")}\\'
            os.rmdir(f'{tempf}\\mcrCache9.11')
            os.system(cmd_str)


class AnalysisUtils:
    '''
    This class does interfaces with LTA outputs.
    The intended use is that each instance refers to a single experiment
    and a single model. Works with LTA class.

    Calling this will run LTA if data needs to be analyzed.
    '''

    def __init__(self, tDir=os.getcwd(), execute=True, **kwargs):
        self.tDir = tDir
        self.execute = execute

        # - - - - - - - - - -
        # init data frame
        if isinstance(tDir, pd.core.frame.DataFrame):  # passed a processed df
            self.model = 'Nan'
            self.df = tDir
        elif 'lifetime_At' in tDir:  # lta output was passed
            self.df, self.model = ReadLTAOut(tDir)
            self.Annotate()
            self.Sort()
        elif '.csv' in tDir:  # a fully processed data set was passed
            self.df, self.model = LoadDF(tDir)
        else:  # a EZRUN dir was passed, thus assumes processing is required
            self.df, self.model = ReadLTAOut(self.ExecuteLTA(**kwargs))
            self.Annotate()
            self.Sort()
        self.ns = GetNScan(self.df)

        # - - - - - - - - - -
        # define relevant attributes of the data
        self.sensors = SetSort(self.df['Sensor'])
        self.tMax, self.aMax = self.UMax()
        self.last = self.FindLast()
        # self.nlvl = self.df['Dosing'].max() + 1
        # self.lvls = np.array(OrderedDict.fromkeys(list(self.df['nomu'])))

    def GetRefTau(self, p):
        '''
        Gets out the reference values and corresponding lifetimes for the given
        set of doses

        pass
            df: dataframe
            p: array of doses under study

        return
            x: array of probe O2 values [nsen, ch, lvls]
            y: array of lifetimes [nsen, ch, lvls]
        '''

        df = Subselect(self.df, {"Dosing": p})
        nlvl = len(p)
        nch = 3
        refCh = 3

        y = df.t1.values.reshape([-1, nch, nlvl])
        x = df.dno.values.reshape([-1, nch, nlvl])
        refCh -= 1
        for i in range(nch):
            if i == refCh:
                continue
            x[:, i, :] = x[:, refCh, :]

        return x, y

    def ExecuteLTA(self, **kwargs) -> str:
        """
        Houses the logic behind whether or not LTA should kick off or not.
        TIMESTAMPS is compared to the LTA processed csv. If theres a mismatch
        of sensors scanned to sensors timestamped, LTA will operate.

        Pass it LTA kwargs, returns the file path of the processed csv

        Beware the occasional mismatch that will happen when a test terminates
        after timestamp is recorded but before data is taken.

        We don't want LTA to run every time, as MATLAB run times are expensive
        """
        L = LTA(self.tDir, **kwargs)

        if self.execute:
            print('Running LTA')
            L.Run()
        return L.fileCSV

    def PxKey(self):
        """
        Reads the dataframe and parses out how many pixels and decays we have.
        Returns a tuple of arrays of keys to said info. ie:

        ktau = np.array([decay, pixel]) -> '<U16'
        TO DO
        """
        None

    def TimeStamps(self, tfile=None) -> pd.core.frame.DataFrame:
        # default is to look in the test folder
        # assumes that EZSetup was used to generate everything
        if tfile is None:
            tfile = self.tDir.split('\\')[-1]
            tfile = f'{self.tDir}\\{tfile}_TIMESTAMPS.csv'
        t = pd.read_csv(tfile)

        # get rid of dupes, keep latest only
        t = t[~t.duplicated('scan', keep='last')]

        # reorganize
        t = t.set_index('scan')
        return t

    def Annotate(self) -> None:
        """
        Appends the dataframe with additional useful info, such as the config
        tags, sensor, channel, and iteration.
        """
        def SenChIt():
            # sensor, channel, and iteration labels
            # init arrays
            sen = np.empty(len(self.df), dtype='<U256')
            chn = np.empty_like(sen, dtype='<U256')
            itr = np.empty_like(sen, dtype=np.int32)
            sid = np.empty(len(self.df), dtype='<U256')
            crd = np.empty_like(sen, dtype=np.int32)
            sin = np.empty_like(sen, dtype=np.int32)
            sub = np.empty_like(sen, dtype='<U256')

            for i, ind in enumerate(self.df.index):
                s, it = ind.split('_')
                itr[i] = int(it)
                if any(cc in s for cc in CH):
                    s, ch = s.rsplit('-', 1)
                else:  # if no channel name
                    ch = 'Nan'
                sen[i] = s
                chn[i] = ch

                if s.count('-') == 2:
                    sid[i] = s.rsplit('-', 1)[0]
                    crd[i], sin[i], sub[i] = s.split('-')
                else:
                    sid[i] = s
                    crd[i], sin[i] = s.split('-')
                    sub[i] = 'O'

            self.df['Sensor'] = sen
            self.df['Ch'] = chn
            self.df['I'] = itr
            self.df['Sensor ID'] = sid
            self.df['Card'] = crd
            self.df['Sensor N'] = sin
            self.df['Subsensor'] = sub

        def Groups():
            # reads in groups and annotates to dataset
            sensors = SetSort(self.df['Sensor'])
            sensors = [i[:8] for i in sensors]
            gdict = ReadConfigTable(sensors)

            tag_card = np.empty(len(self.df), dtype='<U256')
            tag_sensor = np.empty_like(tag_card)
            tag_u = np.empty_like(tag_card)

            for i, ind in enumerate(self.df.index):
                # print(self.df['Sensor'][ind])
                tag_card[i] = gdict[self.df['Sensor'][ind][:8]][0]
                tag_sensor[i] = gdict[self.df['Sensor'][ind][:8]][1]
                tag_u[i] = f'{tag_card[i]}; {tag_sensor[i]}'

            self.df['tag_sensor'] = tag_sensor
            self.df['tag_card'] = tag_card
            self.df['tag_u'] = tag_u

        # timestamp and FMS data
        self.df['Timestamp'] = self.TimeStamps()['time'][self.df.index]
        SenChIt()  # sensor, channel, iteration
        Groups()
        for s in ['upt', 'upo', 'upg', 'upp', 'upf',
                  'dnt', 'dno', 'dng', 'dnp', 'dnf']:
            self.df[s] = self.TimeStamps()[s][self.df.index]

    def Sort(self) -> None:
        """
        sorts sensors by sensor and channel, for example:

        11111-01-A-Hpr_I0
        11111-01-A-Hpr_I1
        ...
        11111-01-A-Eug_I0
        11111-01-A-Eug_I1
        ...
        11112-18-A-Ref_I7s
        """
        sensors = natsort.natsorted(SetSort(self.df['Sensor']))
        order = []

        for s in sensors:
            for c in CH:
                mask = (self.df['Ch'] == c) & (self.df['Sensor'] == s)
                # print(natsort.natsorted(list(self.df[mask].index)))
                order += natsort.natsorted(list(self.df[mask].index))
        # print(order)
        self.df = self.df.reindex(index=order)

    def FindLast(self) -> int:
        size_old = 0
        for i in range(self.df['I'].max() + 1):
            size = len(self.df[self.df['I'] == i])
            if size < size_old:
                return i - 1
            size_old = size
        return i

    def UMax(self):
        """
        returns a unified max for tau and amp for a dataset
        """
        tMax = self.df['t1'].max()

        if tMax > 150:
            tMax = 180
        elif tMax > 90:
            tMax = 150
        else:
            tMax = 90

        aMax = self.df['A1'].max()
        aMax = int(aMax / 1000) + (aMax % 1000 > 0)
        aMax *= 1000
        return tMax, aMax


class TestUtils:
    """
    Treat it pretty much like a container of attributes.
    """

    def __init__(self, tDir):
        self.dfDir = f'{NAS}\\Processed'

        if '.csv' in tDir:
            s = tDir.split('-')
            self.ezid = s[1]
            self.ezrun = s[2]
        else:
            s = tDir.split('\\')
            # 'N:\\EZ_Data\\EZ_205\\RawData\\EZRUN_202110251425'
            for i in s:
                if i == 'EZ_DATA':
                    None
                elif 'EZ_' in i:
                    self.ezid = i
                elif 'EZRUN_' in i:
                    self.ezrun = i


class Parke:
    """
    https://github.com/kriventsov/Clarke-and-Parkes-Error-Grids
    """

    def PYLines(self, ax, zone, c='k', corners=False):
        if corners:
            ax.scatter(zone[:, 0], zone[:, 1], c=c)
        # zone = np.append(zone, zone[0]).reshape([-1, 2])
        ax.plot(zone[:, 0], zone[:, 1], c=c, lw=1, alpha=0.5)

    def PlotGroupLabel(self, ax):
        if ax.get_ylim()[1] > 500:
            ax.text(450, 500, 'A', fontsize=13)
            ax.text(500, 450, 'A', fontsize=13)
            ax.text(300, 500, 'B', fontsize=13)
            ax.text(500, 300, 'B', fontsize=13)
            ax.text(175, 500, 'C', fontsize=13)
            ax.text(500, 175, 'C', fontsize=13)
            ax.text(75, 500, 'D', fontsize=13)
            ax.text(500, 75, 'D', fontsize=13)
            ax.text(15, 500, 'E', fontsize=13)

        else:
            ax.text(325, 360, 'A', fontsize=13)
            ax.text(360, 325, 'A', fontsize=13)
            ax.text(225, 360, 'B', fontsize=13)
            ax.text(360, 225, 'B', fontsize=13)
            ax.text(135, 360, 'C', fontsize=13)
            ax.text(360, 125, 'C', fontsize=13)
            ax.text(65, 360, 'D', fontsize=13)
            ax.text(360, 40, 'D', fontsize=13)
            ax.text(15, 360, 'E', fontsize=13)

    def PlotType1(self, ax):
        G1 = []
        G1 += [np.array([  # E
            [0, 150],
            [35, 155],
            [50, 550]])]
        G1 += [np.array([  # DL
            [0, 100],
            [25, 100],
            [50, 125],
            [80, 215],
            [125, 550]])]
        G1 += [np.array([  # DR
            [250, 0],
            [250, 40],
            [550, 150]])]
        G1 += [np.array([  # CL
            [0, 60],
            [30, 60],
            [50, 80],
            [70, 110],
            [260, 550]])]
        G1 += [np.array([  # CR
            [120, 0],
            [120, 30],
            [250, 130],
            [550, 250]])]
        G1 += [np.array([  # BL
            [0, 50],
            [30, 50],
            [140, 170],
            [280, 380],
            [430, 550]])]
        G1 += [np.array([  # BR
            [50, 0],
            [50, 30],
            [170, 145],
            [385, 300],
            [550, 450]])]
        G1 += [np.array([  # Unity
            [0, 0],
            [550, 550]])]

        for i in G1:
            self.PYLines(ax, i)
        self.PlotGroupLabel(ax)
        ax.text(335, 10, 'Type 1')

    def PlotType2(self, ax):
        G2 = []
        G2 += [np.array([  # E
            [0, 200],
            [35, 200],
            [50, 550]])]
        G2 += [np.array([  # DL
            [0, 80],
            [25, 80],
            [35, 90],
            [125, 550]])]
        G2 += [np.array([  # DR
            [250, 0],
            [250, 40],
            [410, 110],
            [550, 160]])]
        G2 += [np.array([  # CL
            [0, 60],
            [30, 60],
            [280, 550]])]
        G2 += [np.array([  # CR
            [90, 0],
            [260, 130],
            [550, 250]])]
        G2 += [np.array([  # BL
            [0, 50],
            [30, 50],
            [230, 330],
            [440, 550]])]
        G2 += [np.array([  # BR
            [50, 0],
            [50, 30],
            [90, 80],
            [330, 230],
            [550, 450]])]
        G2 += [np.array([  # Unity
            [0, 0],
            [550, 550]])]

        for i in G2:
            self.PYLines(ax, i)
        self.PlotGroupLabel(ax)
        ax.text(300, 10, 'Type 2')

    def CalcMard(self, ref, cal, referr=0.1, ax=None, loc=(50, 410), c='r'):
        ref = ref.reshape(-1)
        cal = cal.reshape(-1)

        # count and drop nans
        drop = np.isnan(cal)
        fdrop = 100 * np.sum(drop) / len(drop)
        ref = ref[~drop]
        cal = cal[~drop]

        # math is fun
        rolls = 1 + np.random.normal(0, referr / 1.96, [2500, len(ref)])
        ref = (ref * rolls)

        ard = 100 * np.abs(cal - ref) / ref

        mard = np.mean(ard)
        smard = np.std(ard)

        if ax is not None:
            txt = f'{np.round(mard, 1)}' + r'$\pm$' + f'{np.round(smard, 1)}'
            txt = r'MARD: ' + txt
            ax.text(*loc, txt, c=c, fontsize=12)

        return mard, smard, fdrop
